package nl.arjan.sandbox.jwe.model;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;

import java.util.UUID;

@Data
@Builder
public class KeyVault {
    @Id
    private final UUID id;
    private final String publicKey;
}
