package nl.arjan.sandbox.jwe.model;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;

import java.time.LocalDateTime;
import java.util.UUID;

@Builder
@Data
public class ShareRequest {
    @Id
    private final UUID shareRequestId;
    private final UUID dataVaultEntryId;
    private final UUID consumerVaultId;
    private final LocalDateTime created;
    private ShareStatus shareStatus;

    public enum ShareStatus {
        NEW, SHARED
    }
}
