package nl.arjan.sandbox.jwe.model;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;

import java.time.LocalDateTime;
import java.util.UUID;

@Builder
@Data
public class DataKey {
    @Id
    private final UUID keyId;
    @Indexed(unique = true, sparse = true)
    private final UUID vaultId;
    private final String encryptedKey;
    private final String publicKey;
    private final LocalDateTime created;
}
