package nl.arjan.sandbox.jwe.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;

import java.time.LocalDateTime;
import java.util.UUID;

@Data
@Builder
public class ConsumerVaultEntry {
    @Id
    private final ConsumerVaultEntryId id;
    private final String jweContent;
    private final LocalDateTime expiryDate;

    @Data
    @AllArgsConstructor
    static public class ConsumerVaultEntryId {
        private final UUID consumerVaultId;
        private final UUID dataVaultId;
    }
}
