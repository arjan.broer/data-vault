package nl.arjan.sandbox.jwe.model;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;

import java.time.LocalDateTime;
import java.util.UUID;

@Builder
@Data
public class DataVaultEntry {
    @Id
    private final UUID dataVaultEntryId;
    private final String jsonWebEncryption;
    private final LocalDateTime expireDate;
}
