package nl.arjan.sandbox.jwe.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Data key not found")
public class DataKeyNotFoundException extends Exception {
}
