package nl.arjan.sandbox.jwe.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
public class KeyCreationException extends Exception {
    public KeyCreationException(String message, Exception e) {
        super(message, e);
    }
}
