package nl.arjan.sandbox.jwe.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "vault cannot be found")
public class VaultNotFoundException extends Exception {
}
