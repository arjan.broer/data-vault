package nl.arjan.sandbox.jwe.sevice;

import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.jwk.RSAKey;
import lombok.extern.slf4j.Slf4j;
import nl.arjan.sandbox.jwe.exception.DataKeyNotFoundException;
import nl.arjan.sandbox.jwe.exception.KeyNotFoundException;
import nl.arjan.sandbox.jwe.model.ConsumerVaultEntry;
import nl.arjan.sandbox.jwe.model.DataVaultEntry;
import nl.arjan.sandbox.jwe.model.KeyVault;
import nl.arjan.sandbox.jwe.model.ShareRequest;
import nl.arjan.sandbox.jwe.repository.*;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import java.security.interfaces.RSAPublicKey;
import java.text.ParseException;
import java.time.LocalDateTime;
import java.util.UUID;

@Slf4j
@Service
public class DataVaultService {
    private static final int TIME_TO_LIVE_IN_MONTHS = 3;

    private final DataKeyRepository dataKeyRepository;
    private final DataVaultRepository dataVaultRepository;
    private final KeyRepository keyRepository;
    private final ShareRequestRepository shareRequestRepository;
    private final ConsumerVaultRepository consumerVaultRepository;
    private final Encrypter encrypter;

    public DataVaultService(DataKeyRepository dataKeyRepository, DataVaultRepository dataVaultRepository, KeyRepository keyRepository, ShareRequestRepository shareRequestRepository, ConsumerVaultRepository consumerVaultRepository, Encrypter encrypter) {
        this.dataKeyRepository = dataKeyRepository;
        this.dataVaultRepository = dataVaultRepository;
        this.keyRepository = keyRepository;
        this.shareRequestRepository = shareRequestRepository;
        this.consumerVaultRepository = consumerVaultRepository;
        this.encrypter = encrypter;
    }

    public DataVaultEntry createDataVaultEntry(@PathVariable("uuid") UUID dataVaultId, @RequestBody String jsonWebEncryption) throws DataKeyNotFoundException {
        if (!dataKeyRepository.existsById(dataVaultId)) {
            log.debug("data key is not available");
            throw new DataKeyNotFoundException();
        }

        DataVaultEntry dataVaultEntry = DataVaultEntry.builder()
                .dataVaultEntryId(dataVaultId)
                .jsonWebEncryption(jsonWebEncryption)
                .expireDate(LocalDateTime.now().plusMonths(TIME_TO_LIVE_IN_MONTHS))
                .build();
        dataVaultEntry = dataVaultRepository.save(dataVaultEntry);
        return dataVaultEntry;
    }


    public ShareRequest createShareRequest(UUID vaultUuid, UUID consumerUuid) throws KeyNotFoundException {
        log.debug("in createShareRequest");
        if (!dataVaultRepository.existsById(vaultUuid)) {
            log.debug("Data vault not found. Throw KeyNotFoundException up the chain");
            throw new KeyNotFoundException();
        }
        if (!keyRepository.existsById(consumerUuid)) {
            log.debug("Consumer vault not found. Throw Exception up the chain");
            throw new KeyNotFoundException();
        }

        ShareRequest shareRequest = ShareRequest.builder()
                .shareRequestId(UUID.randomUUID())
                .consumerVaultId(consumerUuid)
                .dataVaultEntryId(vaultUuid)
                .created(LocalDateTime.now())
                .shareStatus(ShareRequest.ShareStatus.NEW)
                .build();

        return shareRequestRepository.save(shareRequest);
    }

    public void copyData(UUID shareRequestId, RSAKey dataPrivateKey) throws KeyNotFoundException, DataKeyNotFoundException, JOSEException, ParseException {
        ShareRequest shareRequest = shareRequestRepository.findById(shareRequestId).orElseThrow(KeyNotFoundException::new);
        DataVaultEntry dataVaultEntry = dataVaultRepository.findById(shareRequest.getDataVaultEntryId()).orElseThrow(DataKeyNotFoundException::new);
        String jsonWebEncryption = dataVaultEntry.getJsonWebEncryption();
        String payload = encrypter.decrypt(jsonWebEncryption, dataPrivateKey.toRSAPrivateKey());

        KeyVault keyVault = keyRepository.findById(shareRequest.getConsumerVaultId()).orElseThrow(DataKeyNotFoundException::new);
        RSAPublicKey consumerPublicKey = RSAKey.parse(keyVault.getPublicKey()).toRSAPublicKey();

        String encryptedPayload = encrypter.encrypt(payload, consumerPublicKey);

        ConsumerVaultEntry consumerVaultEntry = ConsumerVaultEntry.builder()
                .id(new ConsumerVaultEntry.ConsumerVaultEntryId(shareRequest.getConsumerVaultId(), shareRequest.getDataVaultEntryId()))
                .jweContent(encryptedPayload)
                .expiryDate(LocalDateTime.now().plusMonths(TIME_TO_LIVE_IN_MONTHS))
                .build();

        shareRequest.setShareStatus(ShareRequest.ShareStatus.SHARED);
        shareRequestRepository.save(shareRequest);

        consumerVaultRepository.save(consumerVaultEntry);
    }
}
