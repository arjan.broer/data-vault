package nl.arjan.sandbox.jwe.sevice;


import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.jose4j.jwe.ContentEncryptionAlgorithmIdentifiers;
import org.jose4j.jwe.JsonWebEncryption;
import org.jose4j.jwe.KeyManagementAlgorithmIdentifiers;

import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;

@Slf4j
public class Jose4JEncrypter implements Encrypter {

    @SneakyThrows
    @Override
    public String encrypt(String payload, RSAPublicKey publicKey) {
        JsonWebEncryption jwe = new JsonWebEncryption();
        long start = System.currentTimeMillis();
        jwe.setPayload(payload);
        jwe.setAlgorithmHeaderValue(KeyManagementAlgorithmIdentifiers.RSA_OAEP_256);
        jwe.setEncryptionMethodHeaderParameter(ContentEncryptionAlgorithmIdentifiers.AES_128_CBC_HMAC_SHA_256);

        jwe.setKey(publicKey);
        String compactSerialization = jwe.getCompactSerialization();
        long time = System.currentTimeMillis() - start;
        log.debug("encrypted in {} milis", time);
        return compactSerialization;
    }


    @SneakyThrows
    @Override
    public String decrypt(String compactSerialization, RSAPrivateKey privateKey) {
        JsonWebEncryption jwe = new JsonWebEncryption();
        long start = System.currentTimeMillis();
        jwe.setCompactSerialization(compactSerialization);
        jwe.setKey(privateKey);

        String payload = jwe.getPayload();
        long time = System.currentTimeMillis() - start;
        log.debug("decrypted in {} milis", time);

        return payload;
    }
}
