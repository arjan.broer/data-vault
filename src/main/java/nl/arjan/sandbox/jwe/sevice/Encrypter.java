package nl.arjan.sandbox.jwe.sevice;

import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;

public interface Encrypter {
    String encrypt(String payload, RSAPublicKey publicKey);

    String decrypt(String compactSerialization, RSAPrivateKey privateKey);
}
