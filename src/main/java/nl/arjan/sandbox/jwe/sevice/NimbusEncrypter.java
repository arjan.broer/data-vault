package nl.arjan.sandbox.jwe.sevice;

import com.nimbusds.jose.*;
import com.nimbusds.jose.crypto.RSADecrypter;
import com.nimbusds.jose.crypto.RSAEncrypter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.text.ParseException;

@Slf4j
@Service
public class NimbusEncrypter implements Encrypter {
    @Override
    public String encrypt(String payload, RSAPublicKey publicKey) {
        JWEHeader header = new JWEHeader(JWEAlgorithm.RSA_OAEP_256, EncryptionMethod.A128CBC_HS256);

        JWEObject jweObject = new JWEObject(header, new Payload(payload));
        RSAEncrypter rsaEncrypter = new RSAEncrypter(publicKey);
        try {
            jweObject.encrypt(rsaEncrypter);
        } catch (JOSEException e) {
            log.debug("error occurred while encrypting. Throwing up the chain.", e);
            throw new RuntimeException("unable to encrypt", e);
        }
        return jweObject.serialize();
    }

    @Override
    public String decrypt(String compactSerialization, RSAPrivateKey privateKey) {
        JWEObject jweObject;
        try {
            jweObject = JWEObject.parse(compactSerialization);
        } catch (ParseException e) {
            throw new RuntimeException("Unable to parse", e);
        }
        try {
            jweObject.decrypt(new RSADecrypter(privateKey));
        } catch (JOSEException e) {
            throw new RuntimeException("Unable to decrypt", e);
        }
        return jweObject.getPayload().toString();
    }
}
