package nl.arjan.sandbox.jwe.sevice;

import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.jwk.KeyUse;
import com.nimbusds.jose.jwk.RSAKey;
import com.nimbusds.jose.jwk.gen.RSAKeyGenerator;
import lombok.extern.slf4j.Slf4j;
import nl.arjan.sandbox.jwe.exception.KeyCreationException;
import nl.arjan.sandbox.jwe.exception.VaultNotFoundException;
import nl.arjan.sandbox.jwe.model.DataKey;
import nl.arjan.sandbox.jwe.model.KeyVault;
import nl.arjan.sandbox.jwe.repository.DataKeyRepository;
import nl.arjan.sandbox.jwe.repository.KeyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.security.interfaces.RSAPublicKey;
import java.text.ParseException;
import java.time.LocalDateTime;
import java.util.UUID;
import java.util.stream.Collectors;

@Slf4j
@Service
public class KeyService {
    private final KeyRepository keyRepository;
    private final DataKeyRepository dataKeyRepository;
    private final Encrypter encrypter;

    @Value("classpath:jwk_private_key.json")
    Resource privateKeyFile;

    @Autowired
    public KeyService(KeyRepository keyRepository, DataKeyRepository dataKeyRepository, Encrypter encrypter) {
        this.keyRepository = keyRepository;
        this.dataKeyRepository = dataKeyRepository;
        this.encrypter = encrypter;
    }

    public UUID createKeyVault(RSAKey publicKey) {
        UUID keyVaultId = UUID.randomUUID();
        KeyVault keyVault = KeyVault.builder().id(keyVaultId).publicKey(publicKey.toJSONString()).build();
        keyRepository.save(keyVault);
        return keyVaultId;
    }

    public RSAKey createDataKey(UUID vaultUuid) throws KeyCreationException, VaultNotFoundException, JOSEException, ParseException {
        KeyVault keyVault = keyRepository.findById(vaultUuid).orElseThrow(VaultNotFoundException::new);
        RSAKey rsaDataKey = createRsaKey();
        RSAPublicKey keyVaultEncryptionKey = RSAKey.parse(keyVault.getPublicKey()).toRSAPublicKey();
        String encryptedJWK = encrypter.encrypt(rsaDataKey.toJSONString(), keyVaultEncryptionKey);
        DataKey dataKey = DataKey.builder()
                .vaultId(vaultUuid)
                .keyId(UUID.fromString(rsaDataKey.getKeyID()))
                .encryptedKey(encryptedJWK)
                .publicKey(rsaDataKey.toJSONString())
                .created(LocalDateTime.now())
                .build();
        dataKeyRepository.save(dataKey);
        return rsaDataKey;
    }

    private RSAKey createRsaKey() throws KeyCreationException {
        RSAKeyGenerator rsaKeyGenerator = new RSAKeyGenerator(2048);
        RSAKey rsaDataKey;
        try {
            rsaDataKey = rsaKeyGenerator
                    .keyUse(KeyUse.ENCRYPTION)
                    .keyID(UUID.randomUUID().toString())
                    .generate();
        } catch (JOSEException e) {
            throw new KeyCreationException("Unable to generate key", e);
        }
        return rsaDataKey;
    }

    public RSAKey getPublicVaultKey() throws ParseException, IOException {
        RSAKey parse = getPrivateVaultKey();
        return parse.toPublicJWK();
    }

    private RSAKey getPrivateVaultKey() throws IOException, ParseException {
        InputStreamReader inputStreamReader = new InputStreamReader(privateKeyFile.getInputStream());
        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
        String rsaKeyJson = bufferedReader.lines().collect(Collectors.joining());

        return RSAKey.parse(rsaKeyJson);
    }

    public RSAKey unlockKey(String encryptedKey) throws IOException, ParseException, JOSEException {
        RSAKey privateVaultKey = getPrivateVaultKey();
        String decrypt = encrypter.decrypt(encryptedKey, privateVaultKey.toRSAPrivateKey());
        return RSAKey.parse(decrypt);
    }
}
