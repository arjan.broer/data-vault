package nl.arjan.sandbox.jwe.endpoint;

import com.nimbusds.jose.jwk.RSAKey;
import lombok.extern.slf4j.Slf4j;
import nl.arjan.sandbox.jwe.endpoint.viewmodel.VaultRegistration;
import nl.arjan.sandbox.jwe.exception.VaultNotFoundException;
import nl.arjan.sandbox.jwe.model.ConsumerVaultEntry;
import nl.arjan.sandbox.jwe.repository.ConsumerVaultRepository;
import nl.arjan.sandbox.jwe.sevice.KeyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.util.UUID;

@Slf4j
@RestController
@RequestMapping("/consumer-vault")
public class ConsumerVaultEndpoint {

    private final KeyService keyService;
    private final ConsumerVaultRepository consumerVaultRepository;

    @Autowired
    public ConsumerVaultEndpoint(KeyService keyService, ConsumerVaultRepository consumerVaultRepository) {
        this.keyService = keyService;
        this.consumerVaultRepository = consumerVaultRepository;
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public VaultRegistration register(@RequestBody String jsonKeySpec) throws ParseException {
        log.trace("in register");
        RSAKey parsedKey = RSAKey.parse(jsonKeySpec);
        log.debug(parsedKey.toJSONObject().toJSONString());
        return VaultRegistration.builder().vaultId(keyService.createKeyVault(parsedKey)).build();
    }

    @GetMapping("/{consumerVaultId}/{dataVaultEntryId}")
    public String collectData(@PathVariable("consumerVaultId") UUID consumerVaultId, @PathVariable("dataVaultEntryId") UUID dataVaultEntryId) throws VaultNotFoundException {
        log.debug("collecting data for consumer {} with dataId {}", consumerVaultId, dataVaultEntryId);

        ConsumerVaultEntry consumerVaultEntry = consumerVaultRepository.findById(new ConsumerVaultEntry.ConsumerVaultEntryId(consumerVaultId, dataVaultEntryId)).orElseThrow(VaultNotFoundException::new);
        return consumerVaultEntry.getJweContent();
    }
}
