package nl.arjan.sandbox.jwe.endpoint;

import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.jwk.RSAKey;
import lombok.extern.slf4j.Slf4j;
import net.minidev.json.JSONObject;
import nl.arjan.sandbox.jwe.exception.KeyCreationException;
import nl.arjan.sandbox.jwe.endpoint.viewmodel.VaultRegistration;
import nl.arjan.sandbox.jwe.exception.KeyNotFoundException;
import nl.arjan.sandbox.jwe.exception.VaultNotFoundException;
import nl.arjan.sandbox.jwe.model.KeyVault;
import nl.arjan.sandbox.jwe.repository.KeyRepository;
import nl.arjan.sandbox.jwe.sevice.KeyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.util.UUID;

@Slf4j
@RestController
@RequestMapping("/key-vaults")
public class KeyVaultEndpoint {

    private final KeyService keyService;
    private final KeyRepository keyRepository;

    @Autowired
    public KeyVaultEndpoint(KeyService keyService, KeyRepository keyRepository) {
        this.keyService = keyService;
        this.keyRepository = keyRepository;
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public VaultRegistration register(@RequestBody String jsonKeySpec) throws ParseException {
        log.trace("in register");
        RSAKey parsedKey = RSAKey.parse(jsonKeySpec);
        return VaultRegistration.builder().vaultId(keyService.createKeyVault(parsedKey)).build();
    }

    @GetMapping(path = "/{uuid}", produces = MediaType.APPLICATION_JSON_VALUE)
    public KeyVault retrieveKeyVault(@PathVariable("uuid")UUID uuid) throws KeyNotFoundException {
        return keyRepository.findById(uuid).orElseThrow(KeyNotFoundException::new);
    }

    @GetMapping(value = "/{uuid}/dataEncryptionKey", produces = MediaType.APPLICATION_JSON_VALUE)
    public JSONObject getDataEncryptionKey(@PathVariable("uuid")UUID vaultUuid) throws KeyCreationException, VaultNotFoundException, JOSEException, ParseException {
        RSAKey dataKey = keyService.createDataKey(vaultUuid);
        return dataKey.toPublicJWK().toJSONObject();
    }
}
