package nl.arjan.sandbox.jwe.endpoint.viewmodel;

import lombok.Builder;
import lombok.Data;
import net.minidev.json.JSONObject;

import java.util.UUID;

@Builder
@Data
public class ShareRequestKeys {
    private final UUID shareRequestId;
    private final String encryptedKey;
    private final JSONObject vaultPublicKey;
}
