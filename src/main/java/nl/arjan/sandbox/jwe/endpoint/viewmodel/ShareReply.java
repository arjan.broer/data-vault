package nl.arjan.sandbox.jwe.endpoint.viewmodel;

import lombok.Builder;
import lombok.Data;

import java.util.UUID;

@Data
@Builder
public class ShareReply {
    private final UUID shareRequestId;
    private final UUID dataVaultId;
    private final UUID consumerVaultId;
}
