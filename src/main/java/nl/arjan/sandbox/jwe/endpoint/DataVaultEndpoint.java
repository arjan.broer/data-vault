package nl.arjan.sandbox.jwe.endpoint;

import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JWEObject;
import com.nimbusds.jose.jwk.RSAKey;
import lombok.extern.slf4j.Slf4j;
import nl.arjan.sandbox.jwe.endpoint.viewmodel.ShareReply;
import nl.arjan.sandbox.jwe.endpoint.viewmodel.ShareRequestKeys;
import nl.arjan.sandbox.jwe.exception.DataKeyNotFoundException;
import nl.arjan.sandbox.jwe.exception.InvalidJweException;
import nl.arjan.sandbox.jwe.exception.KeyNotFoundException;
import nl.arjan.sandbox.jwe.model.DataKey;
import nl.arjan.sandbox.jwe.model.DataVaultEntry;
import nl.arjan.sandbox.jwe.model.ShareRequest;
import nl.arjan.sandbox.jwe.repository.DataKeyRepository;
import nl.arjan.sandbox.jwe.repository.ShareRequestRepository;
import nl.arjan.sandbox.jwe.sevice.DataVaultService;
import nl.arjan.sandbox.jwe.sevice.KeyService;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Slf4j
@RestController
@RequestMapping("/datavaults")
public class DataVaultEndpoint {

    private final DataVaultService dataVaultService;
    private final KeyService keyService;

    private final ShareRequestRepository shareRequestRepository;
    private final DataKeyRepository dataKeyRepository;

    public DataVaultEndpoint(DataVaultService dataVaultService, KeyService keyService, ShareRequestRepository shareRequestRepository, DataKeyRepository dataKeyRepository) {
        this.dataVaultService = dataVaultService;
        this.keyService = keyService;
        this.shareRequestRepository = shareRequestRepository;
        this.dataKeyRepository = dataKeyRepository;
    }

    @PutMapping("/{uuid}")
    public DataVaultEntry storeData(@PathVariable("uuid") UUID dataVaultId, @RequestBody String jsonWebEncryption) throws InvalidJweException, DataKeyNotFoundException {
        log.debug("about to store data in data vault {}", dataVaultId.toString());
        try {
            JWEObject.parse(jsonWebEncryption);
        } catch (ParseException e) {
            log.debug("unable to parse the provided JWE, throw exception up the chain");
            throw new InvalidJweException();
        }

        return dataVaultService.createDataVaultEntry(dataVaultId, jsonWebEncryption);
    }

    @PutMapping("/{vaultUuid}/share/{consumerUuid}")
    public ShareReply requestForShare(@PathVariable("vaultUuid") UUID vaultUuid, @PathVariable("consumerUuid") UUID consumerUuid) throws KeyNotFoundException {
        ShareRequest shareRequest = dataVaultService.createShareRequest(vaultUuid, consumerUuid);
        return ShareReply.builder()
                .shareRequestId(shareRequest.getShareRequestId())
                .dataVaultId(shareRequest.getDataVaultEntryId())
                .consumerVaultId(shareRequest.getConsumerVaultId())
                .build();
    }

    @GetMapping("/{uuid}/share-requests")
    public List<ShareRequestKeys> shareRequests(@PathVariable("uuid") UUID dataVaultId) throws KeyNotFoundException, IOException, ParseException {
        log.debug("get shareRequests for {}", dataVaultId.toString());
        List<ShareRequest> allByDataVaultEntryId = shareRequestRepository.findAllByDataVaultEntryId(dataVaultId);
        log.debug("found {} shareRequests", allByDataVaultEntryId.size());
        List<ShareRequestKeys> shareRequestKeys = new ArrayList<>();
        for (ShareRequest shareRequest : allByDataVaultEntryId) {
            DataKey dataKey = dataKeyRepository.findById(shareRequest.getDataVaultEntryId()).orElseThrow(KeyNotFoundException::new);
            ShareRequestKeys requestKeys = ShareRequestKeys.builder()
                    .shareRequestId(shareRequest.getShareRequestId())
                    .encryptedKey(dataKey.getEncryptedKey())
                    .vaultPublicKey(keyService.getPublicVaultKey().toJSONObject())
                    .build();
            shareRequestKeys.add(requestKeys);
        }
        return shareRequestKeys;
    }

    @PostMapping("/share/{shareRequestId}")
    public void share(@PathVariable("shareRequestId") UUID shareRequestId,
                      @RequestBody ShareRequestKeys shareRequestKeys) throws ParseException, IOException, JOSEException, DataKeyNotFoundException, KeyNotFoundException {
        log.debug("Received approval for {}", shareRequestId);
        RSAKey dataPrivateKey = keyService.unlockKey(shareRequestKeys.getEncryptedKey());

        log.debug("kid of encrypted key  {}", dataPrivateKey.getKeyID());

        // Read the data vault and store it in the other datavault
        dataVaultService.copyData(shareRequestId, dataPrivateKey);

    }

}
