package nl.arjan.sandbox.jwe.repository;

import nl.arjan.sandbox.jwe.model.KeyVault;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface KeyRepository extends MongoRepository<KeyVault, UUID> {
}
