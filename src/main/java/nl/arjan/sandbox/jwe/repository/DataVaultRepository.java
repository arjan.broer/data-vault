package nl.arjan.sandbox.jwe.repository;

import nl.arjan.sandbox.jwe.model.DataVaultEntry;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.UUID;

public interface DataVaultRepository extends MongoRepository<DataVaultEntry, UUID> {
}
