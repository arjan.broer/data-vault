package nl.arjan.sandbox.jwe.repository;

import nl.arjan.sandbox.jwe.model.DataKey;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.UUID;

public interface DataKeyRepository extends MongoRepository<DataKey, UUID> {

}
