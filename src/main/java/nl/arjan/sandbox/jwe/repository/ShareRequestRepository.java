package nl.arjan.sandbox.jwe.repository;

import nl.arjan.sandbox.jwe.model.ShareRequest;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;
import java.util.UUID;

public interface ShareRequestRepository extends MongoRepository<ShareRequest, UUID> {
    List<ShareRequest> findAllByDataVaultEntryId(UUID dataVaultEntryId);
}
