package nl.arjan.sandbox.jwe.repository;

import nl.arjan.sandbox.jwe.model.ConsumerVaultEntry;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ConsumerVaultRepository extends MongoRepository<ConsumerVaultEntry, ConsumerVaultEntry.ConsumerVaultEntryId> {
}
