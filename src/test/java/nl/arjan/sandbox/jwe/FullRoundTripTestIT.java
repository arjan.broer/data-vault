package nl.arjan.sandbox.jwe;

import com.nimbusds.jose.*;
import com.nimbusds.jose.crypto.RSADecrypter;
import com.nimbusds.jose.crypto.RSAEncrypter;
import com.nimbusds.jose.jwk.KeyUse;
import com.nimbusds.jose.jwk.RSAKey;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.interfaces.RSAPublicKey;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class FullRoundTripTestIT {

    public static final String PAYLOAD = "Hello world";

    @Autowired
    private MockMvc mvc;

    @Test
    public void createVault() throws Exception {

        RSAKey ownerKey = createOwnerKey();
        JSONObject ownerKeyCreationResponse = jsonPost(ownerKey.toJSONString());
        String ownerVaultId = ownerKeyCreationResponse.getString("vaultId");

        RSAKey consumerKey = createOwnerKey();
        JSONObject comsumerKeyCreationResponse = jsonPost(consumerKey.toJSONString());
        String consumerVaultId = comsumerKeyCreationResponse.getString("vaultId");

        // Get the encrypyion key via GET /{ownerVaultId}/dataEncryptionKey
        RSAKey encryptionKey = getRsaPublicKey("/key-vaults/" + ownerVaultId + "/dataEncryptionKey");

        // send the data via PUT /datavaults/{ownerVaultId}

        JWEHeader header = new JWEHeader(JWEAlgorithm.RSA_OAEP_256, EncryptionMethod.A128CBC_HS256);
        JWEObject jwe = new JWEObject(header, new Payload(PAYLOAD));
        RSAEncrypter encrypter = new RSAEncrypter(encryptionKey);
        jwe.encrypt(encrypter);
        UUID dataVaultId = UUID.fromString(encryptionKey.getKeyID());
        mvc.perform(put("/datavaults/" + dataVaultId.toString())
                .contentType("application/json").content(jwe.serialize()))
                .andExpect(status().isOk());

        // create share request via PUT /{dataVaultId}/share/{consumerVaultId}
        mvc.perform(put("/datavaults/" + dataVaultId.toString() + "/share/" + consumerVaultId))
                .andExpect(status().isOk());

        // approve share request via
        String serializedShareRequests = mvc.perform(get("/datavaults/" + dataVaultId + "/share-requests"))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();
        JSONArray shareRequests = new JSONArray(serializedShareRequests);
        JSONObject shareRequestKeysetJSON = shareRequests.getJSONObject(0);
        String encryptedKey = shareRequestKeysetJSON.getString("encryptedKey");
        RSAKey vaultPublicKey = RSAKey.parse(shareRequestKeysetJSON.getJSONObject("vaultPublicKey").toString());

        JWEObject jweDataKey = JWEObject.parse(encryptedKey);
        jweDataKey.decrypt(new RSADecrypter(ownerKey));

        JWEObject returnDataKey = new JWEObject(header, jweDataKey.getPayload());
        returnDataKey.encrypt(new RSAEncrypter(vaultPublicKey));

        JSONObject returnDataKeySet = new JSONObject();
        returnDataKeySet.put("encryptedKey", returnDataKey.serialize());
        returnDataKeySet.put("vaultPublicKey", shareRequestKeysetJSON.getJSONObject("vaultPublicKey"));
        String shareRequestId = shareRequestKeysetJSON.getString("shareRequestId");
        returnDataKeySet.put("shareRequestId", shareRequestId);

        mvc.perform(post("/datavaults/share/" + shareRequestId)
                .contentType("application/json")
                .content(returnDataKeySet.toString()))
                .andExpect(status().isOk());

        // Get the content from the consumer vault
        String consumerJWE = mvc.perform(get("/consumer-vault/" + consumerVaultId + "/" + dataVaultId))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        JWEObject consumerJWEObject = JWEObject.parse(consumerJWE);
        consumerJWEObject.decrypt(new RSADecrypter(consumerKey));
        assertEquals(PAYLOAD, consumerJWEObject.getPayload().toString());
    }

    private RSAKey getRsaPublicKey(String url) throws Exception {
        String serializedRsaKey = mvc.perform(get(url)
                .contentType("application/json"))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();
        return RSAKey.parse(serializedRsaKey);
    }

    private JSONObject jsonPost(String content) throws Exception {
        String responseContent = mvc.perform(post("/key-vaults")
                .contentType("application/json")
                .content(content))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();
        return new JSONObject(responseContent);
    }

    private RSAKey createOwnerKey() throws NoSuchAlgorithmException {
        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
        keyPairGenerator.initialize(2048);
        KeyPair keyPair = keyPairGenerator.generateKeyPair();
        return new RSAKey.Builder((RSAPublicKey) keyPair.getPublic())
                .privateKey(keyPair.getPrivate())
                .keyUse(KeyUse.ENCRYPTION)
                .keyID("encrypt")
                .build();
    }
}
