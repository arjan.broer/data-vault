package nl.arjan.sandbox.jwe;

import lombok.extern.slf4j.Slf4j;
import nl.arjan.sandbox.jwe.sevice.Jose4JEncrypter;
import org.jose4j.jwk.JsonWebKey;
import org.jose4j.jwk.JsonWebKeySet;
import org.jose4j.jwk.RsaJsonWebKey;
import org.jose4j.jwk.RsaJwkGenerator;
import org.jose4j.lang.IntegrityException;
import org.jose4j.lang.JoseException;
import org.junit.Before;
import org.junit.Test;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import static org.junit.Assert.*;

@Slf4j
public class Jose4JEncrypterTest {

    private static RsaJsonWebKey rsaJsonWebKey;
    private static RsaJsonWebKey wrongKey;

    @Before
    public void setup() throws JoseException {
        rsaJsonWebKey = RsaJwkGenerator.generateJwk(2048);

        wrongKey = RsaJwkGenerator.generateJwk(2048);
    }

    @Test
    public void encryptionRoundtrip() throws JoseException {
        Jose4JEncrypter jose4JEncrypter = new Jose4JEncrypter();
        String content = "hello world";
        String encrypted = jose4JEncrypter.encrypt(content, rsaJsonWebKey.getRsaPublicKey());
        String payload = jose4JEncrypter.decrypt(encrypted, rsaJsonWebKey.getRsaPrivateKey());

        log.debug("payload : {}", payload);

        assertEquals(content, payload);
    }

    @Test(expected = IntegrityException.class)
    public void wrongKeyEncryptionRoundtrip() throws JoseException {
        Jose4JEncrypter jose4JEncrypter = new Jose4JEncrypter();
        String content = "hello world";
        String encrypted = jose4JEncrypter.encrypt(content, rsaJsonWebKey.getRsaPublicKey());
        jose4JEncrypter.decrypt(encrypted, wrongKey.getRsaPrivateKey());
    }

    @Test
    public void largerRoundTrip() throws JoseException, IOException {

        Jose4JEncrypter jose4JEncrypter = new Jose4JEncrypter();
        String content = readFromFile("testText.txt");
        String encrypted = jose4JEncrypter.encrypt(content, rsaJsonWebKey.getRsaPublicKey());
        String payload = jose4JEncrypter.decrypt(encrypted, rsaJsonWebKey.getRsaPrivateKey());

        log.debug("payload : {}", payload);

        assertEquals(content, payload);
    }

    private String readFromFile(String filename) throws IOException {
        return new String(Files.readAllBytes(Paths.get(filename)));
    }

//    @Test
    void generateKeyFiles() throws JoseException {
        RsaJsonWebKey rsaKey = RsaJwkGenerator.generateJwk(2048);
        JsonWebKeySet keyset = new JsonWebKeySet(rsaKey);
        String privateKey = keyset.toJson(JsonWebKey.OutputControlLevel.INCLUDE_PRIVATE);
        String publicKey = keyset.toJson(JsonWebKey.OutputControlLevel.PUBLIC_ONLY);

        String fileName = "rsa_key.json";
        writeToFile(privateKey, fileName);
        writeToFile(publicKey, "rsa_pub.json");
    }

    private void writeToFile(String privateKey, String fileName) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(fileName))) {
            writer.write(privateKey);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}