package nl.arjan.sandbox.jwe;

import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.jwk.KeyUse;
import com.nimbusds.jose.jwk.RSAKey;
import lombok.extern.slf4j.Slf4j;
import nl.arjan.sandbox.jwe.sevice.Encrypter;
import nl.arjan.sandbox.jwe.sevice.NimbusEncrypter;
import org.junit.Before;
import org.junit.Test;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.interfaces.RSAPublicKey;

import static org.junit.Assert.*;

@Slf4j
public class NimbusEncrypterTest {

    private RSAKey rsaKeyPair;

    @Before
    public void setUp() throws NoSuchAlgorithmException {
        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
        keyPairGenerator.initialize(2048);
        KeyPair keyPair = keyPairGenerator.generateKeyPair();
        rsaKeyPair = new RSAKey.Builder((RSAPublicKey) keyPair.getPublic())
                .privateKey(keyPair.getPrivate())
                .keyUse(KeyUse.ENCRYPTION)
                .keyID("encrypt")
                .build();
    }

    @Test
    public void encrypt() throws JOSEException {
        Encrypter encrypter = new NimbusEncrypter();
        String payload = "Hello world";

        String encrypted = encrypter.encrypt(payload, rsaKeyPair.toRSAPublicKey());
        log.debug("encrypted {}", encrypted);
        String decrypted = encrypter.decrypt(encrypted,rsaKeyPair.toRSAPrivateKey());
        log.debug("decrypted {}", decrypted );
        assertEquals(payload, decrypted);
    }

    @Test
    public void decrypt() {
    }
}