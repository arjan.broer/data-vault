# JWE Data Vault

The data vault is designed to store data with Json Web Encryption. Principle behind the JWE is that the private key is never store in the vault.

## Building the application
``` shell script
mvn clean install
```

## Run the application locally
```shell script
mvn spring-boot:run
```
If you do not have a mongo database available, you can start a docker container with the following command
```shell script
docker run -p 27017:27017 mongo:latest
```
To access the swagger documentation access [Swagger ui](http://localhost:8080/swagger-ui.html "Swagger UI")